package com.example.s00rk.coppeldemo.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.s00rk.coppeldemo.R;
import com.example.s00rk.coppeldemo.data.db.ArticleBean;
import com.example.s00rk.coppeldemo.ui.main.Articles.ArticlesFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainContract.View{

    private Fragment currentFragment;

    private ArrayList<ArticleBean> articles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        changeFragment(ArticlesFragment.newInstance(this), R.string.fragment_articles_title);
    }

    @Override
    public void onBackPressed() {
        if (currentFragment instanceof ArticlesFragment)
            super.onBackPressed();
        else
            changeFragment(ArticlesFragment.newInstance(this), R.string.fragment_articles_title);
    }

    //region MainContract.View

    @Override
    public void setArticles(ArrayList<ArticleBean> articles) {
        this.articles = articles;
    }

    @Override
    public ArrayList<ArticleBean> getArticles() {
        return articles;
    }

    @Override
    public void changeFragment(Fragment fragment, int title) {
        changeFragment(fragment, getString(title));
    }

    @Override
    public void changeFragment(Fragment fragment, String title) {
        currentFragment = fragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, fragment).commit();
        getSupportActionBar().setTitle(title);
    }

    //endregion

}
