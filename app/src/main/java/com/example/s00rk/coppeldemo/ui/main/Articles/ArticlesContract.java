package com.example.s00rk.coppeldemo.ui.main.Articles;


import com.example.s00rk.coppeldemo.data.db.ArticleBean;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by s00rk on 21/06/17.
 */

public interface ArticlesContract {

    interface View {

        void showError();

        void setupArticles(ArrayList<ArticleBean> articles);

    }

    interface Presenter {

        void loadArticles(int width);
        void showErrorLoadingArticles();
        void loadedArticles(JSONArray jsonArray) throws Exception;

        void onItemClick(ArticleBean sku);

    }

}
