package com.example.s00rk.coppeldemo.ui.main.Articles;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.s00rk.coppeldemo.Application;
import com.example.s00rk.coppeldemo.BuildConfig;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by s00rk on 21/06/17.
 */

public class ArticlesInteractor {

    public static void getArticles(ArticlesContract.Presenter presenter) {
        JsonObjectRequest request = new JsonObjectRequest(BuildConfig.URL, new JSONObject(), jsonObject -> {
            try {
                if(jsonObject.has("meta")) {
                    JSONObject jsonObject1 = jsonObject.getJSONObject("meta");
                    if (jsonObject1.has("status") && jsonObject1.getString("status").equals("SUCCESS")) {
                        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONObject("response").getJSONArray("listado");
                        presenter.loadedArticles(jsonArray);
                        return;
                    }
                }
            }catch (Exception ignored) {}
            presenter.showErrorLoadingArticles();
        }, volleyError -> {
            presenter.showErrorLoadingArticles();
        });
        Application.addRequest(request);
    }

}
