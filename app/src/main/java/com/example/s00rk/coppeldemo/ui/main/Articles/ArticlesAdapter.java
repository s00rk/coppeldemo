package com.example.s00rk.coppeldemo.ui.main.Articles;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.s00rk.coppeldemo.ArticlesDataBinding;
import com.example.s00rk.coppeldemo.data.db.ArticleBean;

import java.util.ArrayList;

/**
 * Created by s00rk on 21/06/17.
 */

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ArticleBean> list;
    private ArticlesContract.Presenter presenter;
    private LayoutInflater inflater;

    ArticlesAdapter(Context context, ArrayList<ArticleBean> list, ArticlesContract.Presenter presenter) {
        this.context = context;
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (inflater == null) {
            inflater = LayoutInflater.from(context);
        }
        ArticlesDataBinding articlesDataBinding = ArticlesDataBinding.inflate(inflater, viewGroup, false);
        return new ViewHolder(articlesDataBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        ArticleBean articleBean = list.get(i);
        viewHolder.bind(articleBean);

        final ArticlesDataBinding articlesDataBinding = viewHolder.getmDataBinding();
        articlesDataBinding.setHandler(presenter);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ArticlesDataBinding mDataBinding;

        public ViewHolder(ArticlesDataBinding dataBinding) {
            super(dataBinding.getRoot());
            mDataBinding = dataBinding;
        }

        public void bind(ArticleBean articleBean) {
            mDataBinding.setViewModel(articleBean);
        }

        public ArticlesDataBinding getmDataBinding() {
            return mDataBinding;
        }
    }
}
