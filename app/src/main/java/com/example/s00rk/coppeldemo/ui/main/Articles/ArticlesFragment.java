package com.example.s00rk.coppeldemo.ui.main.Articles;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.s00rk.coppeldemo.R;
import com.example.s00rk.coppeldemo.data.db.ArticleBean;
import com.example.s00rk.coppeldemo.ui.main.MainContract;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class ArticlesFragment extends Fragment implements ArticlesContract.View {

    private ArticlesContract.Presenter presenter;
    private MainContract.View mainContract;
    private MaterialDialog mDialog;

    //region Views
    private RecyclerView recyclerView;
    //endregion

    public static ArticlesFragment newInstance(MainContract.View mainContract) {
        ArticlesFragment fragment = new ArticlesFragment();
        fragment.mainContract = mainContract;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_articles, container, false);

        presenter = new ArticlesPresenter(this, mainContract);

        recyclerView = (RecyclerView) view.findViewById(R.id.articles_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        if(mainContract.getArticles() == null || mainContract.getArticles().size() == 0)
            view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    mDialog = new MaterialDialog.Builder(getActivity())
                            .content(R.string.fragment_articles_loading)
                            .cancelable(false)
                            .canceledOnTouchOutside(false)
                            .autoDismiss(false)
                            .progress(true, 0)
                            .show();
                    presenter.loadArticles(view.getWidth());
                }
            });
        else
            recyclerView.setAdapter(new ArticlesAdapter(getActivity(), mainContract.getArticles(), presenter));

        return view;
    }

    //region ArticlesContract.View

    @Override
    public void showError() {
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
        new MaterialDialog.Builder(getActivity())
                .content(R.string.fragment_articles_error_loading)
                .show();
    }

    @Override
    public void setupArticles(ArrayList<ArticleBean> articles) {
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
        mainContract.setArticles(articles);
        recyclerView.setAdapter(new ArticlesAdapter(getActivity(), articles, presenter));
    }

    //endregion
}
