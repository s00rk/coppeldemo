package com.example.s00rk.coppeldemo.ui.main.Article;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.s00rk.coppeldemo.ArticleDataBinding;
import com.example.s00rk.coppeldemo.R;
import com.example.s00rk.coppeldemo.data.db.ArticleBean;

/**
 * Created by s00rk on 21/06/17.
 */

public class ArticleFragment extends Fragment implements ArticleContract.View {

    private ArticleBean articleBean;

    ArticleDataBinding dataBinding;
    private boolean barcodeGenerated;

    public static ArticleFragment newInstance(ArticleBean articleBean) {
        ArticleFragment fragment = new ArticleFragment();
        fragment.articleBean = articleBean;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        barcodeGenerated = articleBean.barCode != null;
        dataBinding = ArticleDataBinding.inflate(inflater, container, false);
        dataBinding.setViewModel(articleBean);
        dataBinding.setHandler(this);
        dataBinding.setShowBarcode(false);

        return dataBinding.getRoot();
    }


    //region ArticleContract.View

    @Override
    public void showBarcode(Boolean show) {
        if(barcodeGenerated)
            dataBinding.setShowBarcode(!show);
    }

    //endregion

}
