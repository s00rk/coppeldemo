package com.example.s00rk.coppeldemo.utils;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.EAN13Writer;

/**
 * Created by s00rk on 21/06/17.
 */

public class Utilities {

    @BindingAdapter("imgSrc")
    public static void setImgSrc(ImageView imageView, String url) {
        if(url != null)
            Glide.with(imageView).load(url).into(imageView);
    }

    @BindingAdapter("imgSrcBitmap")
    public static void setImgSrcBitmap(ImageView imageView, Bitmap bitmap) {
        if(bitmap != null)
            imageView.setImageBitmap(bitmap);
    }

    public static Bitmap generateBarCode(String data, int width) {
        while(data.length() < 12) {
            data = "0" + data;
        }
        if(data.length() == 12)
            data += checkSum(data);
        Log.d("utilities", data);
        MultiFormatWriter c9 = new MultiFormatWriter();
        Bitmap mBitmap = null;
        try {
            BitMatrix bm = c9.encode(data, BarcodeFormat.EAN_13, width, 350);
            mBitmap = Bitmap.createBitmap(width, 350, Bitmap.Config.ARGB_8888);

            for (int i = 0; i < width; i++) {
                for (int j = 0; j < 350; j++) {

                    mBitmap.setPixel(i, j, bm.get(i, j) ? Color.BLACK : Color.WHITE);
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
        if (mBitmap != null) {
            return mBitmap;
        }
        return null;
    }

    private static int checkSum (String code){
        int val=0;
        for(int i=0;i<code.length();i++){
            val+=((int)Integer.parseInt(code.charAt(i)+""))*((i%2==0)?1:3);
        }

        int checksum_digit = 10 - (val % 10);
        if (checksum_digit == 10) checksum_digit = 0;

        return checksum_digit;
    }

}
