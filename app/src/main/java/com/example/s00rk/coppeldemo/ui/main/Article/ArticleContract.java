package com.example.s00rk.coppeldemo.ui.main.Article;

import android.graphics.Bitmap;

import com.example.s00rk.coppeldemo.data.db.ArticleBean;

/**
 * Created by s00rk on 21/06/17.
 */

public interface ArticleContract {

    interface View {

        void showBarcode(Boolean show);

    }

    interface Presenter {

    }

}
