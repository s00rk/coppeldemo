package com.example.s00rk.coppeldemo.ui.main.Articles;

import com.example.s00rk.coppeldemo.R;
import com.example.s00rk.coppeldemo.data.db.ArticleBean;
import com.example.s00rk.coppeldemo.ui.main.Article.ArticleFragment;
import com.example.s00rk.coppeldemo.ui.main.MainContract;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by s00rk on 21/06/17.
 */

public class ArticlesPresenter implements ArticlesContract.Presenter {

    private ArticlesContract.View view;
    private MainContract.View mainContract;
    private int widthView;

    ArticlesPresenter(ArticlesContract.View view, MainContract.View mainContract) {
        this.view = view;
        this.mainContract = mainContract;
    }

    //region ArticlesContract.Presenter

    @Override
    public void loadArticles(int width) {
        widthView = width;
        ArticlesInteractor.getArticles(this);
    }

    @Override
    public void loadedArticles(JSONArray jsonArray) throws Exception {
        ArrayList<ArticleBean> articleBeans = new ArrayList<>();
        for(int x = 0; x < jsonArray.length(); x++) {
            JSONObject jsonObject = jsonArray.getJSONObject(x);
            ArticleBean articleBean = new ArticleBean(jsonObject, widthView);
            articleBeans.add(articleBean);
        }

        view.setupArticles(articleBeans);
    }

    @Override
    public void showErrorLoadingArticles() {
        view.showError();
    }

    @Override
    public void onItemClick(ArticleBean articleBean) {
        mainContract.changeFragment(ArticleFragment.newInstance(articleBean), articleBean.description);
    }

    //endregion
}
