package com.example.s00rk.coppeldemo.ui.main;

import android.support.v4.app.Fragment;

import com.example.s00rk.coppeldemo.data.db.ArticleBean;

import java.util.ArrayList;

/**
 * Created by s00rk on 21/06/17.
 */

public interface MainContract {

    interface View {

        void setArticles(ArrayList<ArticleBean> articles);
        ArrayList<ArticleBean> getArticles();

        void changeFragment(Fragment fragment, int title);
        void changeFragment(Fragment fragment, String title);

    }

}
