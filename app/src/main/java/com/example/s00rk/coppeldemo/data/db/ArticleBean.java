package com.example.s00rk.coppeldemo.data.db;

import android.databinding.ObservableInt;
import android.graphics.Bitmap;

import org.json.JSONObject;

import static com.example.s00rk.coppeldemo.utils.Utilities.generateBarCode;

/**
 * Created by s00rk on 21/06/17.
 */

public class ArticleBean {

    public String description;
    public int sku;
    public double price;
    public String image;
    public Bitmap barCode;

    private final String KEY_DESCRIPTION = "descripcion_articulo";
    private final String KEY_SKU = "sku";
    private final String KEY_PRICE = "precio";
    private final String KEY_IMAGE = "imagen";

    public ArticleBean(JSONObject jsonObject, int width) throws Exception {
        if (jsonObject.has(KEY_DESCRIPTION))
            description = jsonObject.getString(KEY_DESCRIPTION);
        if (jsonObject.has(KEY_SKU)) {
            sku = jsonObject.getInt(KEY_SKU);
            barCode = generateBarCode(String.valueOf(sku), width);
        }
        if (jsonObject.has(KEY_PRICE))
            price = jsonObject.getDouble(KEY_PRICE);
        if (jsonObject.has(KEY_IMAGE))
            image = jsonObject.getString(KEY_IMAGE);
    }


    public String getPriceFormated() {
        return "$" + String.valueOf(price);
    }
}
